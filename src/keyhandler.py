import pygame
from pygame.locals import *

keylisteners = []

def register(func):
    keylisteners.append(func)

def handle(event):
    for func in keylisteners:
        func(event)
