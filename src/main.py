import pdb

import sys, pygame
from pygame.locals import *
pygame.init()

import entity
import text
import character
import keyhandler
import timerhandler

from scene import GameScene, MainMenuScene

screensize = width, height = 640, 576

display = pygame.display.set_mode(screensize)

blue = 0, 0, 255

current_scene = GameScene(width, height)

clock = pygame.time.Clock()

#display.fill(blue)
pygame.display.flip()

#TODO: HUDs, add GameScene objects to drawable and update list
#   In the latter case: Put them in a loop and iterate over it.
#   Bonus points for figuring out the Tiled map import.
#   (But of course, I'll want to mutate it as the game progresses.)
#   (Maybe I can make Tiled maps for the changes and layer 'em?)
while 1:
    clock.tick(60)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE): 
            sys.exit()
        if((event.type == KEYDOWN) or (event.type == KEYUP)):
            keyhandler.handle(event)
        
    
    dt = clock.get_time()
    timerhandler.updateTimers(dt)
    current_scene.update(dt)

    display.fill(blue)
    dirty_rects = current_scene.draw_all(display)
    pygame.display.update(dirty_rects) 
		
