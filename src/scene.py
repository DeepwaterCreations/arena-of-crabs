import pygame
from pygame.locals import *

import entity
import text
import character

from drawable import loadImage
from character import Character
from cricket import Cricket
from hud import Hud
from death_message import DeathMessage
from crabspawner import CrabSpawner
from background import Background
from decoration import WallTorch 

class Scene:

    def __init__(self, width, height):
        self.scene_drawables = pygame.sprite.LayeredUpdates()
        self.scene_updatables = []

        self.width = width
        self.height = height

    def add_updatable(self, updatable):
        self.scene_updatables.append(updatable)

    def update(self, dt):
        for thingy in self.scene_updatables:
            try:
                thingy.update(dt)
            except AttributeError:
                print("Un-updatable updatable: {}".format(thingy))

    def add_drawable(self, drawable):
        self.scene_drawables.add(drawable)

    def draw_all(self, surface):
        for whatever in self.scene_drawables:
            if whatever.isVisible():
                whatever.updateImage()
        return self.scene_drawables.draw(surface)




#I don't need 'em to register themselves because the scene
#modules will downright create them.
#TODO: Make sure it's straightforward for the scenes to talk
#between themselves if necessary. I want an "inventory" scene as well.


class MainMenuScene(Scene):

    def __init__(self, width, height):
        super().__init__(width, height)
        # screen_elements = []
        # bg_img = loadImage("", use_transparency = False)

        # bg_sprite = Drawable("Hud_BG")
        # bg_sprite.image = bg_img
        # bg_sprite.setVisible(False)

class GameScene(Scene):

    def __init__(self, width, height):
        super().__init__(width, height)

        #TODO: Load map elements from Tiled file (which can be json! Sweet, yeah?)
        self.top_wall = entity.Wall(0, 0, self.width, 64)
        self.add_drawable(self.top_wall) #TODO: And so on
        self.bottom_wall = entity.Wall(0, self.height - 128, self.width, 64)
        self.left_wall = entity.Wall(0, 64, 64, self.height - 128)
        self.right_wall = entity.Wall(self.width - 64, 64, 64, self.height - 128)
        self.corner_ur = entity.Wall(self.width - 128, 64, 64, 64)
        self.corner_lr = entity.Wall(self.width - 128, self.height - 192, 64, 64)
        self.corner_ul = entity.Wall(64, 64, 64, 64)
        self.corner_ll = entity.Wall(64, self.height - 192, 64, 64)
        self.background = Background()

        self.walltorch_topleft = WallTorch(Character.Direction.UP)
        self.walltorch_topleft.rect.x = 64 * 3
        self.walltorch_topleft.rect.y = 0
        self.walltorch_topright = WallTorch(Character.Direction.UP)
        self.walltorch_topright.rect.x = 64 * 6
        self.walltorch_topright.rect.y = 0
        self.walltorch_righttop = WallTorch(Character.Direction.RIGHT)
        self.walltorch_righttop.rect.x = 64 * 9
        self.walltorch_righttop.rect.y = 64 * 2
        self.walltorch_rightbottom = WallTorch(Character.Direction.RIGHT)
        self.walltorch_rightbottom.rect.x = 64 * 9
        self.walltorch_rightbottom.rect.y = 64 * 5
        self.walltorch_bottomleft = WallTorch(Character.Direction.DOWN)
        self.walltorch_bottomleft.rect.x = 64 * 3
        self.walltorch_bottomleft.rect.y = 64 * 7
        self.walltorch_bottomright = WallTorch(Character.Direction.DOWN)
        self.walltorch_bottomright.rect.x = 64 * 6
        self.walltorch_bottomright.rect.y = 64 * 7
        self.walltorch_lefttop = WallTorch(Character.Direction.LEFT)
        self.walltorch_lefttop.rect.x = 0
        self.walltorch_lefttop.rect.y = 64 * 2
        self.walltorch_leftbottom = WallTorch(Character.Direction.LEFT)
        self.walltorch_leftbottom.rect.x = 0
        self.walltorch_leftbottom.rect.y = 64 * 5

        text.loadTextSprites()

        #TODO: Not like this... not like this.
        #The hud should probs be its own scene, yeah?
        self.cricket = Cricket()
        self.cricket.loadSprites()

        self.crabzone = Rect(self.left_wall.rect.right + 64, self.top_wall.rect.bottom + 64, self.right_wall.rect.left - self.left_wall.rect.width - 128, self.bottom_wall.rect.top - self.top_wall.rect.height - 128)
        self.crabspawner = CrabSpawner(self.crabzone)

        self.hud_space = Rect(0, self.bottom_wall.rect.bottom, self.width, self.height - self.left_wall.rect.height)
        self.hud = Hud(self.hud_space)
        self.hud.registerListeners(self.cricket, self.crabspawner)

        self.game_space = Rect(0, 0, self.width, self.height - self.hud_space.height)
        self.death_message = DeathMessage(self.game_space)
        self.cricket.addCricketDeathListener(self.death_message)

    def update(self, dt):
        super().update(dt)
        for (collider, wall) in pygame.sprite.groupcollide(character.wall_colliders, entity.walls, False, False):
            collider.onWallCollision(wall)
        for enemy in pygame.sprite.spritecollide(self.cricket, character.enemies, False):
            enemy.onPlayerCollision(self.cricket)
